package com.cwiczeniaEnum;

public enum Size {
    SMALL("S","Maly"),MEDIUM("M","Sredni"),
    LARGE("L", "Duzy");

    final String shortName;
    final String polishName;

    Size(String shortName, String polishName) {
        this.shortName = shortName;
        this.polishName = polishName;
    }

    public String getShortName() {
        return shortName;
    }

    public String getPolishName() {
        return polishName;
    }
    static Size fromDescription(String description){
    Size[] values = values();
        for (Size size : values) {
            if(size.getShortName().equals(description) || size.getPolishName().equals(description)) return size;
        }
        return null;
    }

}
