package com.cwiczeniaEnum;

public class Apple extends Fruit {
    private final static String TYPE = "Jablkowaty";
    private String verity;

    Apple(int weight, Size size, String verity) {
        super(weight, TYPE, size);
        this.verity = verity;
    }
    Apple(int weight, String size, String verity) {
        super(weight, TYPE, size);
        this.verity = verity;
    }

    @Override
    String getInfo() {
        return super.getInfo() + " -- Size: " + getSize() +  " -- Kind: " + verity;
    }

    @Override
    public String toString() {
        final String text = " -- Kind: " + verity;
        if (verity.equalsIgnoreCase("Polskie")) return super.toString() + getPolishSize() + text;
        return super.toString() + getShortSize() + text ;
    }
}

