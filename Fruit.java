package com.cwiczeniaEnum;

class Fruit {
    private int weight;
    private String type;
    private Size size;

    Fruit(int weight, String type, Size size) {
        this.weight = weight;
        this.type = type;
        this.size = size;
    }
    Fruit(int weight, String type, String size) {
        this.weight = weight;
        this.type = type;
        this.size = Size.fromDescription(size);
    }

    String getInfo(){
        return "Fruit type: " + type + " Weight: " + weight +"g";
    }
    Size getSize() {
        return size;
    }
    String getShortSize(){
        return size.shortName;
    }
    String getPolishSize(){
        return size.polishName;
    }

    @Override
    public String toString() {
           return  "Fruit type: " + type + " Weight: " + weight +"g"   + " -- Size: ";
    }
}


