package com.cwiczeniaEnum;

public class GroceryShop {
    public static void main(String[] args) {
        Apple apple1 = new Apple( 220, "Sredni","Szampion");
        Apple apple2 = new Apple( 330,"L","Zimowe");
        Apple apple3 = new Apple(230,Size.MEDIUM  ,"Polskie");
        Apple apple4 = new Apple(150, Size.SMALL, "Gala"  );

        Banana banana1 = new Banana(100, "M","Brazil");
        Banana banana2 = new Banana(200, "L","Peru");

        System.out.println(apple1.getInfo());
        System.out.println(apple2.getInfo());
        System.out.println(apple3);
        System.out.println(apple4);
        System.out.println(banana1);
        System.out.println(banana2);
    }
}


