package com.cwiczeniaEnum;

public class Banana extends Fruit{
    private final static String TYPE = "Bananowaty";
    private String importCountry;

    Banana(int weight, String size, String importCountry) {
        super(weight, TYPE, size);
        this.importCountry = importCountry;
    }

    @Override
    public String toString() {
        return super.toString() + getShortSize() + "-- Imported from: " + importCountry;
    }
}
